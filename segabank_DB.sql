--
-- Table structure for table `AGENCE`
--

DROP TABLE IF EXISTS `AGENCE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `AGENCE` (
  `idAGENCE` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idAGENCE`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AGENCE`
--

LOCK TABLES `AGENCE` WRITE;
/*!40000 ALTER TABLE `AGENCE` DISABLE KEYS */;
INSERT INTO `AGENCE` VALUES (1,'NTE44100','rue de la banque');
/*!40000 ALTER TABLE `AGENCE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `COMPTE`
--

DROP TABLE IF EXISTS `COMPTE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `COMPTE` (
  `idCOMPTE` int(11) NOT NULL AUTO_INCREMENT,
  `solde` decimal(12,2) DEFAULT NULL,
  `decouvert` decimal(12,2) DEFAULT NULL,
  `tauxInteret` decimal(4,2) DEFAULT NULL,
  `idAGENCE` int(11) DEFAULT NULL,
  PRIMARY KEY (`idCOMPTE`),
  KEY `FK_IDAGENCE_idx` (`idAGENCE`),
  CONSTRAINT `FK_IDAGENCE` FOREIGN KEY (`idAGENCE`) REFERENCES `agence` (`idAGENCE`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `COMPTE`
--

LOCK TABLES `COMPTE` WRITE;
/*!40000 ALTER TABLE `COMPTE` DISABLE KEYS */;
INSERT INTO `COMPTE` VALUES (1,200.00,NULL,NULL,1),(2,300.00,100.00,NULL,1),(3,500.00,NULL,0.07,1),(6,395.00,0.00,0.00,1),(7,225.00,100.00,0.00,1);
/*!40000 ALTER TABLE `COMPTE` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;