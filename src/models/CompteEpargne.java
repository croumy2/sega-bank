package models;

public class CompteEpargne extends Compte {

    private double tauxInteret;

    public CompteEpargne(int id, double solde, double tauxInteret){
        super(id, solde);
        this.tauxInteret = tauxInteret;
    }

    @Override
    public void retrait(double amount) {
        if (this.solde - amount >= 0){
            this.solde -= amount;
        }
        else {
            System.out.println("Solde insuffisant");
        }
    }

    public void calculInterets(){
        this.solde += estimationInterets();
    }

    public double estimationInterets(){
        return this.solde * this.tauxInteret;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder( "Compte Epargne : \n" );
        sb.append( " Compte n° : " ).append( id ).append("\n");
        sb.append( " Solde : " ).append(solde).append("\n");
        sb.append( " Taux d'intéret : " ).append( tauxInteret ).append("\n");
        sb.append( " Intérêts : " ).append( estimationInterets() ).append("\n");
        return sb.toString();
    }

    public double getTauxInteret() {
        return tauxInteret;
    }

    public void setTauxInteret(double tauxInteret) {
        this.tauxInteret = tauxInteret;
    }
}
