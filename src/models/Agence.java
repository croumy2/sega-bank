package models;

import java.util.ArrayList;
import java.util.List;

public class Agence {
    private int id;
    private String code;
    private String adresse;
    private List<Compte> comptes;

    public Agence(int id, String code, String adresse){
        this.id = id;
        this.code = code;
        this.adresse = adresse;
        this.comptes = new ArrayList<Compte>();
    }

    public Agence(int id, String code, String adresse, List<Compte> comptes) {
        this.id = id;
        this.code = code;
        this.adresse = adresse;
        this.comptes = comptes;
    }

    public Compte getCompte(int idCompte){
        Compte compteSelectionne = null;

        for(Compte compte : this.comptes){
            if(compte.getId() == idCompte){
                compteSelectionne = compte;
            }
        }

        return compteSelectionne;
    }

    public void ajouterCompte(Compte compte){
        this.comptes.add(compte);
    }

    public void modifierCompte(){

    }

    public void supprimerCompte(Compte compte){
        this.comptes.remove(compte);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder( "AGENCE : \n" );
        sb.append( " Agence n° : " ).append( id ).append("\n");
        sb.append( " Code : " ).append(code).append("\n");
        sb.append( " Addresse : " ).append(adresse).append("\n");
        return sb.toString();
    }

    //region GETTERS/SETTERS
    //----------------------------------

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public List<Compte> getComptes() {
        return comptes;
    }

    public void setComptes(List<Compte> comptes) {
        this.comptes = comptes;
    }

    //endregion
}
