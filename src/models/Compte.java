package models;

public abstract class Compte {
    protected int id;
    protected double solde;

    public Compte(int id, double solde){
        this.id = id;
        this.solde = solde;
    }

    public void versement(double amount){
        if (amount > 0){
            this.solde += amount;
        }
        else {
            System.out.println("Le montant ne peut pas être négatif");
        }
    }
    public abstract void retrait(double amount);

    @Override
    public abstract String toString();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getSolde() {
        return solde;
    }

    public void setSolde(double solde) {
        this.solde = solde;
    }
}
