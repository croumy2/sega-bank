package tests;

import models.Compte;
import models.CompteEpargne;

public class TestCompteEpargne {
    public static void main(String[] args) {
        Compte compteEpargne = new CompteEpargne(3, 500.50, 0.10);
        System.out.println(compteEpargne);

        compteEpargne.retrait(300);
        System.out.println(compteEpargne.getSolde());

        compteEpargne.retrait(550);
        System.out.println(compteEpargne.getSolde());

        compteEpargne.versement(100);
        System.out.println(compteEpargne.getSolde());
    }
}
