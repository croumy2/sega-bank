package tests;

import models.Compte;
import models.CompteEpargne;
import models.ComptePayant;
import models.CompteSimple;

public class TestCompteSimple {
    public static void main(String[] args) {
        Compte compteSimple = new CompteSimple(1, 500.50, 150);
        System.out.println(compteSimple);

        compteSimple.retrait(635);
        System.out.println(compteSimple.getSolde());

        compteSimple.retrait(750.5);
        System.out.println(compteSimple.getSolde());

        compteSimple.versement(150);
        System.out.println(compteSimple.getSolde());

        compteSimple.versement(-2);
        System.out.println(compteSimple.getSolde());

    }
}
