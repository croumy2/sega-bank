package tests;

import models.Compte;
import models.ComptePayant;

public class TestComptePayant {
    public static void main(String[] args) {
        Compte comptePayant = new ComptePayant(2, 500.50);
        System.out.println(comptePayant);

        comptePayant.retrait(50);
        System.out.println(comptePayant.getSolde());

        comptePayant.retrait(490);
        System.out.println(comptePayant.getSolde());

        comptePayant.versement(50);
        System.out.println(comptePayant.getSolde());
    }
}
