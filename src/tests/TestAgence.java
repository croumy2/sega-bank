package tests;

import models.*;

import java.util.ArrayList;
import java.util.List;

public class TestAgence {
    public static void main(String[] args) {

        Agence agence = new Agence(1, "NTE44100", "rue de la banque");
        System.out.println(agence);


        Compte compteSimple = new CompteSimple(1, 300, 100);
        Compte comptePayant = new ComptePayant(2, 300);
        Compte compteEpargne = new CompteEpargne(3, 300, 0.5);

        List<Compte> comptes = new ArrayList<Compte>();
        comptes.add(compteEpargne);
        comptes.add(compteSimple);

        Agence agenceAvecCompte = new Agence(2, "ANG49000", "Rue des Lices", comptes);
        System.out.println(agenceAvecCompte);
        System.out.println(agenceAvecCompte.getComptes());

        agence.ajouterCompte(compteSimple);
        agence.ajouterCompte(comptePayant);
        agence.ajouterCompte(compteEpargne);

        System.out.println(agence.getComptes());

        agence.supprimerCompte(compteSimple);

        System.out.println(agence.getComptes());

        System.out.println(agence.getCompte(2));
    }
}
